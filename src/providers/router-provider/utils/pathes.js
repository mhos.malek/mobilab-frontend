export default {
  // INDEX Route
  INDEX: '/',
  SAMPLE_PAGE: '/sample-page',
  // other Routes
  NOT_FOUND: '*',
};
