import React from 'react';
import { Router, Switch } from 'react-router-dom';
import routes from './routes';
import PrivateRoute from './components/private-route';
import BrowserHistory from './utils/browser-history';
import PublicRoute from './components/public-route';
import NotFoundRoute from './components/not-found';
import pathes from './utils/pathes';

const RouterProvider = () => {
  const isAuthenticated = true; //for test porpose I have set It true
  return (
    <>
      <Router history={BrowserHistory}>
        <Switch>
          {routes.map((route, index) => {
            return route.isPrivate ? (
              <PrivateRoute
                isAuthenticated={isAuthenticated}
                index={index}
                path={route.path}
                exact={route.exact || true}
                key={index}
                meta={route.meta}
                hasLayout={route.hasLayout}
                Component={route.component}
              />
            ) : (
              <PublicRoute
                isAuthenticated={isAuthenticated}
                index={index}
                path={route.path}
                exact={route.exact || true}
                key={index}
                meta={route.meta}
                hasLayout={route.hasLayout}
                Component={route.component}
              />
            );
          })}
          <NotFoundRoute
            index={Math.random()}
            path={pathes.NOT_FOUND_PAGE}
            exact={true}
          />
          ;
        </Switch>
      </Router>
    </>
  );
};

export default RouterProvider;
