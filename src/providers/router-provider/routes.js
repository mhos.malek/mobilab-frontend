import PATH_TO from './utils/pathes';

// import page componentes;
import {
  HomePageDetails,
  SamplePageDetails,
  NotFoundPageDetails,
} from '@/ui/pages';

const Routes = [
  {
    path: PATH_TO.INDEX,
    name: 'HomePage',
    component: HomePageDetails.component,
    meta: HomePageDetails.meta,
    isPrivate: true,
    hasLayout: true,
  },

  {
    path: PATH_TO.SAMPLE_PAGE,
    name: 'Sample Page',
    component: SamplePageDetails.component,
    meta: SamplePageDetails.meta,
    isPrivate: false,
    hasLayout: true,
  },

  {
    path: PATH_TO.NOT_FOUND,
    name: 'NotFound',
    component: NotFoundPageDetails.component,
    meta: NotFoundPageDetails.meta,
    hasLayout: false,
    isPrivate: false,
  },
];

export default Routes;
