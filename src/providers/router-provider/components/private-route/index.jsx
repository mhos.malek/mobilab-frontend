import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import pathes from '../../utils/pathes';
import PropTypes from 'prop-types';
import MainLayout from '../../../../ui/layouts';

const PrivateRoute = ({
  isAuthenticated,
  index,
  path,
  exact,
  meta,
  hasLayout,
  Component,
}) => {
  if (isAuthenticated) {
    return hasLayout ? (
      <MainLayout>
        <Route key={index} path={path} exact={exact}>
          {React.cloneElement(<Component />, {
            meta,
          })}
        </Route>
      </MainLayout>
    ) : (
      <Route key={index} path={path} exact={exact}>
        {React.cloneElement(<Component />, {
          meta,
        })}
      </Route>
    );
  }

  return <Redirect to={pathes.SAMPLE_PAGE} />;
};

PrivateRoute.propTypes = {
  Component: PropTypes.any,
  index: PropTypes.number,
  path: PropTypes.string,
  exact: PropTypes.bool,
  isAuthenticated: PropTypes.bool,
  hasLayout: PropTypes.bool,
  meta: PropTypes.any,
};
export default PrivateRoute;
