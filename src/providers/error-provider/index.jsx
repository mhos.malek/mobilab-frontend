import * as React from 'react';
import PropTypes from 'prop-types';
import NOTIFICATION_MESSAGES from './notification-messages';
import ErrorHandler from '.';
class ErrorProvider extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error) {
    // do something with error
    this.setState({ hasError: true });
    ErrorHandler.componentErrorHandler(error);
  }

  render() {
    const { hasError } = this.state;
    const { children } = this.props;
    if (hasError) {
      return <h1>{NOTIFICATION_MESSAGES.ERRORS.ERROR_BOUNDAY}</h1>;
    }
    return children;
  }
}

ErrorProvider.propTypes = {
  children: PropTypes.node,
};
export default ErrorProvider;
