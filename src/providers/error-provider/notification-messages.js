const NOTIFICATION_MESSAGES = {
  ERRORS: {
    DEFAULT_ERROR_BOUNDAY: 'Something went wrong in our app',
    DEFAULT_NETWORK_ERROR: 'Something went wrong in the server',
  },
};

export default NOTIFICATION_MESSAGES;
