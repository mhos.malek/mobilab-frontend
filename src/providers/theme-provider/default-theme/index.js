// this is used when you want to cheage theme with js
const MobilabTheme = () => {
  const colors = {
    primary: '#07cac1',
    white: '#ffffff',
    transparent: 'transparent',
    danger: '#d05354',
    disableColor: '#565656',
  };

  const dimensions = {
    xs: '2px',
    sm: '4px',
    md: '8px',
    lg: '16px',
    xl: '24px',
    xxl: '32px',
    xxxl: '48px',
    xxxxl: '52px',
    xxxxxxxxl: '72px',
    xxxxxl: '80px',
    xxxxxxl: '104px',
    full: '100%',
    fullViewPortWidth: '100vw',
    fullViewPortHeight: '100vh',
  };

  const typography = {
    defaultFont: {
      name: 'Dana',
      regular: {
        16: `
        font-size: 16px;
        font-weight: normal;
        line-height: 2;

       `,
        14: `
        font-size: 14px;
        font-weight: normal;
        line-height: 2;

       `,
        13: `
        font-size: 13px;
        font-weight: normal;
        line-height: 2;

  `,
        12: `
  font-size: 12px;
  font-weight: normal;
  line-height: 2;

`,
      },
      normal: {
        24: `
        font-size: 24px;
        font-weight: 500;
        line-height: 2;

       `,
        18: `
  font-size: 18px;
  font-weight: 500;
  line-height: 2;

  `,
        16: `
  font-size: 16px;
  font-weight: 500;
  line-height: 2;

  `,
      },
      bold: {
        24: `
        font-size: 24px;
        font-weight: bold;
        line-height: 2;
       `,
        16: `
        font-size: 16px;
        font-weight: bold;
        line-height: 2;
       `,
        14: `
  font-size: 14px;
  font-weight: bold;
  line-height: 2;
  `,
      },
    },
    englishFont: {
      name: 'Tahoma',
      regular: {
        16: `
        font-family: Tahoma;
        font-size: 16px;
        font-weight: normal;
        line-height: 2;

       `,
        14: `
        font-family: Tahoma;
        font-size: 14px;
        font-weight: normal;
        line-height: 2;

       `,
        13: `
        font-family: Tahoma;
        font-size: 13px;
        font-weight: normal;
        line-height: 2;

  `,
        12: `
        font-family: Tahoma;
  font-size: 12px;
  font-weight: normal;
  line-height: 2;

`,
      },
      normal: {
        24: `
        font-family: Tahoma;
        font-size: 24px;
        font-weight: 500;
        line-height: 2;

       `,
        18: `
        font-family: Tahoma;
  font-size: 18px;
  font-weight: 500;
  line-height: 2;

  `,
        16: `
        font-family: Tahoma;
  font-size: 16px;
  font-weight: 500;
  line-height: 2;

  `,
      },
      bold: {
        16: `
        font-family: Tahoma;
        font-size: 16px;
        font-weight: bold;
        line-height: 2;
       `,
        14: `
        font-family: Tahoma;
  font-size: 14px;
  font-weight: bold;
  line-height: 2;
  `,
      },
    },
  };

  return {
    colors,
    dimensions,
    typography,
  };
};

export default MobilabTheme;
