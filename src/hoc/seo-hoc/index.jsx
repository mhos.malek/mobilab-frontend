import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
const withSeo = (Component, seoData) => {
  // seo data will be used when you dont have meta data in page and pass them when you pass it when you use HOC
  function withSeoCompoent({ meta, pageTitle }) {
    const { metaContents, mataLinks } = meta || seoData;
    return (
      <>
        <Helmet>
          <meta charSet='utf-8' />
          {/* change paga Title */}
          {pageTitle && <title>{pageTitle}</title>}

          {/* meta contents like  <meta name="description" content="Nested component" /> */}
          {metaContents &&
            metaContents.length > 0 &&
            metaContents.map((metaData, index) => {
              const { name, content } = metaData;
              return <meta key={index} name={name} content={content} />;
            })}

          {/* metalinke like  <link rel="canonical" href="http://mysite.com/example" /> */}
          {mataLinks &&
            mataLinks.length > 0 &&
            mataLinks.map((metaData, index) => {
              const { rel, href } = metaData;
              return <link key={index} rel={rel} href={href} />;
            })}
        </Helmet>
        <Component />
      </>
    );
  }
  withSeoCompoent.defaultProps = {
    pageTitle: 'Mobilab Solutions',
  };
  withSeoCompoent.propTypes = {
    pageTitle: PropTypes.string.isRequired,
    meta: PropTypes.shape({
      metaContents: PropTypes.array,
      mataLinks: PropTypes.array,
    }),
  };
  return withSeoCompoent;
};

export default withSeo;
