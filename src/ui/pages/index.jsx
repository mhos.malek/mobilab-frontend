import HomePage from './home-page';
import SamplePage from './sample-page';
import NotFoundPage from './not-found-page';

// use for SEO and React Helmet
const HomePageDetails = {
  component: HomePage,
  meta: {
    pageTitle: 'Mobilab | Home',
  },
  head: [],
};
const SamplePageDetails = {
  component: SamplePage,
  meta: {
    metaContents: [
      {
        name: 'description',
        content: 'Sample Content for MobileLib',
      },
    ],
  },
  head: [],
};

const NotFoundPageDetails = {
  component: NotFoundPage,
  meta: {},
  head: [],
};

export { HomePageDetails, SamplePageDetails, NotFoundPageDetails };
