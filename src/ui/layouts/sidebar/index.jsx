import React, { useState, useContext } from 'react';
import * as styles from './style.scss';
import Card from 'react-bootstrap/Card';
import Icon from '../../base-components/icon';
import MobilabThemeContext from '../../../providers/theme-provider/context';
import classNames from 'classnames';
import FilterImages from '../../smart-components/filter';
import WithOutsideClickHandler from '../../base-components/outsideClick';
const SideBar = () => {
  const [isOpen, setIsOpen] = useState(false);
  const theme = useContext(MobilabThemeContext);
  const sideBarStyle = {
    right: `${isOpen ? 0 : '-100%'}`,
  };
  const buttonStyle = {
    left: `${isOpen ? -30 : -100}px`,
  };
  const getIconColor = () => (isOpen ? theme.colors.primary : null);

  return (
    <WithOutsideClickHandler onOutsideClick={() => setIsOpen(false)}>
      <aside className={styles.wrapper}>
        <Card style={sideBarStyle} className={styles.sideBar}>
          <Card.Body>
            <div className={styles.iconWrapper}>
              <Icon name='adjustments' color={getIconColor()} size='1.5rem' />
            </div>

            <Card.Title>
              <span> Want to have more controll?</span>
            </Card.Title>
            <FilterImages />
          </Card.Body>
          <button
            onClick={() => setIsOpen(prevState => !prevState)}
            className={classNames(styles.toggleButton, {
              'rotate-animation': isOpen,
            })}
            style={buttonStyle}
          >
            <Icon name='adjustments' color={getIconColor()} size='1rem' />
            <span style={{ color: getIconColor() }}>Filter</span>
          </button>
        </Card>
      </aside>
    </WithOutsideClickHandler>
  );
};

export default SideBar;
