// dependenices
import React from 'react';
import classnames from 'classnames';
// base components
import FilterImages from '../../smart-components/filter';

// styles
import * as styles from './style.scss';

const TopbarFilters = () => {
  return (
    <div className={classnames('d-flex align-items-center', styles.topFilter, styles.hideInMobile)}>
      <FilterImages inlineForm />
    </div>
  );
};
export default TopbarFilters;
