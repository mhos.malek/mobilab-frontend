import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
// I have created special icons from IcoMoon Website
import './style.scss';

const Icon = ({ name, size, color, rotate, onClick, className }) => {
  const style = {
    fontSize: size || '1em',
    color,
    display: 'inline-block',
    transform: `rotate(${rotate}deg)`,
    transition: 'all 0.3s',
  };
  return (
    <span
      className={classnames(`icon-${name}`, className)}
      style={style}
      onClick={onClick && onClick}
    />
  );
};

Icon.propTypes = {
  name: PropTypes.string,
  size: PropTypes.string,
  color: PropTypes.string,
  rotate: PropTypes.number,
  onClick: PropTypes.func,
  className: PropTypes.string,
};

export default Icon;
