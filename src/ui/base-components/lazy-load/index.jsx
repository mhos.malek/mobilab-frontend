import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import LazyLoadPlaceholder from './lazy-load-placeholder';

const LazyLoad = ({ children, placeholder, onItemShownOnViewPort }) => {
  const lazyLoadRefElement = useRef();
  const lazyObserver = useRef();
  const isIntersectionObserverSupported = 'IntersectionObserver' in window;

  const [isElementOnViewPort, setIsElementOnViewPort] = useState(false);
  useEffect(() => {
    startWatchingElement();
    return function() {
      const { current } = lazyLoadRefElement;
      lazyObserver.current.unobserve(current);
    };
  }, []);

  const startWatchingElement = () => {
    if (isIntersectionObserverSupported) {
      observeElement();
    }
  };
  const observeElement = () => {
    const { current } = lazyLoadRefElement;
    lazyObserver.current = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          setIsElementOnViewPort(true);
          onItemShownOnViewPort && onItemShownOnViewPort();
          lazyObserver.current.unobserve(current);
        }
      });
    });
    lazyObserver.current.observe(current);
  };
  return (
    <div ref={lazyLoadRefElement}>
      {!isElementOnViewPort ? <div> {placeholder} </div> : children}
    </div>
  );
};

LazyLoad.propTypes = {
  children: PropTypes.node.isRequired,
  placeholder: PropTypes.element,
  onItemShownOnViewPort: PropTypes.func,
};

LazyLoad.defaultProps = {
  placeholder: <LazyLoadPlaceholder />,
};

export default LazyLoad;
