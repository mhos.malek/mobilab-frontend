// dependencies
import React from 'react';
import PropTypes from 'prop-types';

// ui component
import Card from 'react-bootstrap/Card';
import Carousel from 'react-bootstrap/Carousel';

// base component
import VideoPlayer from '@/ui/base-components/video';
import Icon from '@/ui/base-components/icon';
import { VIDEO_TYPE, IMAGE_TYPE } from '../fileTypes';

const MultipleItems = ({ imagesList, onCarouselChange }) => {
  const carouselOptions = {
    interval: null,
    onSelect: index => onCarouselChange && onCarouselChange(index),
    nextIcon: (
      <Icon name='chevron-down' rotate={270} color='#07cac1' size='2rem' />
    ),
    prevIcon: (
      <Icon name='chevron-down' rotate={-270} color='#07cac1' size='2rem' />
    ),
  };

  const renderImage = (src, title) => {
    return <Card.Img variant='top' alt={title} src={src} />;
  };
  const renderVideo = src => {
    return <VideoPlayer src={src} controls />;
  };

  return imagesList.length > 1 ? (
    <Carousel {...carouselOptions}>
      {imagesList.map((imageOrVideo, index) => {
        const { type, link, title } = imageOrVideo;
        return (
          <Carousel.Item key={index}>
            {IMAGE_TYPE.includes(type) && renderImage(link, title)}
            {type === VIDEO_TYPE && renderVideo(link)}
          </Carousel.Item>
        );
      })}
    </Carousel>
  ) : IMAGE_TYPE.includes(imagesList[0].type) ? (
    renderImage(imagesList[0].link)
  ) : (
    renderVideo(imagesList[0].link)
  );
};

MultipleItems.propTypes = {
  imagesList: PropTypes.array,
  onCarouselChange: PropTypes.func,
};

export default MultipleItems;
