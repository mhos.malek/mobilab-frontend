const fetcher = url =>
  fetch(url, {
    headers: {
      Authorization: `Client-ID ${process.env.IMGUR_CLIENT_ID}`,
    },
  }).then(r => r.json());

export default fetcher;
