import React from 'react';
import ReactDOM from 'react-dom';
import RootProvider from './providers';

ReactDOM.render(<RootProvider />, document.getElementById('app'));
